#!/usr/bin/env bash
#created by: Ilay Ron
#purpose: create partion on device
#date :19/10/2020
#version: v1.0.4
######################################

main(){
count=0
local disk_name="$1"
local disk_size="$2"

if [[ -z $disk_name ]];then
    help
    exit 1
else
parts=$(check_amount_of_parttions $disk_name)
while [[ $parts>$count ]]
    do
        delete_part $disk_name
        let count++
    done

create_part $disk_name
mkfs_on_disk_mount $disk_name

deco "Done"

fi
}

deco(){
_time=2.5
l="####################"
clear
printf "$l\n# %s\n$l" "$@"
sleep $_time
clear
}

help(){

if [[ -z $@ ]];then
   deco  "please provide disk name or disk size or both"
   exit 1
fi
}


create_part(){
local disk_name="$1"
local disk_size="$2"

if [[ -z $disk_name ]] && [[ -z $disk_size ]];then
    help
    exit 1
else
fdisk $disk_name << EOL
n
p


+$disk_size
w
EOL

fi

}



check_amount_of_parttions(){
local disk_name="$1"
if [[ -z $disk_name ]];then
    help
    exit 1
else
    amount_of_partitions=$(fdisk  -l $disk_name|grep  -A100 "Device"|wc -l)
    echo $amount_of_partitions
fi
}

mkfs_on_disk_mount(){
local disk_name="$@"
_time=2.5
if [[ -z $disk_name ]];then
    deco "unable to mount: no disk provided"
    exit 1
else
    disk_part=$(fdisk -l $disk_name|awk '{print $1}'|grep '/dev')
    mkfs.ext4 $disk_part
    sleep $_time
    mkdir -p /mnt/$disk_part
    mount $disk_part /mnt$disk_part

fi
}



main "$@"

